package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class DurationTest {

        @Test
        public void testDescription() throws Exception {
                final String result = new Duration().dur();
                assertTrue("Description contains CP-DOF", result.contains("CP-DOF"));
        }

}

