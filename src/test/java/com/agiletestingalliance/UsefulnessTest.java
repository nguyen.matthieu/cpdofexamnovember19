package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class UsefulnessTest {

        @Test
        public void testDescription() throws Exception {
                final String result = new Usefulness().desc();
                assertTrue("Description contains CP-DOF", result.contains("CP-DOF"));
        }

}

