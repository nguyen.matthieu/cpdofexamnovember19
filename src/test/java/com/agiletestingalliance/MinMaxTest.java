package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class MinMaxTest {

        @Test
        public void testFirstOperandBigger() throws Exception {
                final int result = new MinMax().max(5,2);
                assertEquals("Max(5,2)", 5 , result);
        }

        @Test
        public void testSecondOperandBigger() throws Exception {
                final int result = new MinMax().max(2,5);
                assertEquals("Max(2,5)", 5 , result);
        }



}

